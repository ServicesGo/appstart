<?php 
/*
http://app.dev

https://appstartphp.appspot.com

*/

/*
$sqlhost = "10.0.75.1";//docker default
$sqluser = "root";
$sqlpass = "docker";
*/

// connect to mysql database
$sqlhost = getenv('MYSQL_DSN');
$sqluser = getenv('MYSQL_USER');
$sqlpass = getenv('MYSQL_PASSWORD');
$sqldb = "appstart";

$mysqli = mysqli_connect($sqlhost,$sqluser,$sqlpass, $sqldb);
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit($sqldb.' connection error ');
}

$sql = "SELECT * FROM users";
if($result = $mysqli->query($sql)){
	while($row = $result->fetch_object()) {
		$users .= "<h1>$row->fname $row->lname</h1>";
	}
}

echo"<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>$sqldb</title>
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Raleway:200,300,400'>
<link rel='shortcut icon' href='assets/image/icons/favicon.ico' type='image/x-icon' />

<style type='text/css'>
body { margin: 0px; font: normal 14pt/140% Raleway, Helvetica, sans-serif; color:#000; background:#eee; }
#container { width:50%; margin:0 auto; padding:50px; text-align:center;  box-shadow: 0 0 20px 5px #888888; font-weight:300; background:#fff}
	a:hover { color:#808080; }
	h1 { font-size:24pt; font-weight:200; color:#000;}
	h2 { font-size:22pt; font-weight:200; color:#000;}
	h3 { font-size:20pt; font-weight:200; color:#000;}
	p { padding: 5px 0px 10px 0px; }
	.btn {
	  color: #ffffff;
	  font-size: 35px;
	  background: #44A7EB;
	  padding: 15px 25px 15px 25px;
	  text-decoration: none;
	}
	.btn:hover {color: #ffffff;background: #3cb0fd;text-decoration: none;}
</style>
</head>
<body>
<div id='container'>

	<img src='assets/image/logo.png' width='250' height='250' border='0' alt=''>

	<h1><strong>Thanks to:</strong></h1>

	<h1>$sqldb</h1>

	<p>$users</p>

	<h1><a class='btn' href='phpinfo.php'>PHP Info</a></h1>

</div>
</body>
</html>";
?>